# school-ms-project

> Call center platform using VoIP. It powered by Voice over IP protocol, which makes it posible to directly make connection with usual phone number.
It provides ability to work with team, storing and reviewing call information and more.

### Stack
 - spring
 - nodejs
 - Containerization


### High-level overview
![design](/images/call-center-design.jpeg)


### Bounded Context

* User Service 
    - stores and manages user profiles
* Project Service
    - stores project information
    - provides project crud operations
* Call Service
    - stores call information
    - provides call crud operations
* Ticket Service
    - stores customer information
    - creates ticket lists for easy usage
    - provides crud operations for tickets and ticket lists
* Project Metrics Service
    - collects some (to be determined) information about project
    - provides metrics on succeed and failed sales
    - provides metrics on client's interests
* Payment Service
    - provides payment functionality for users


### User Stories

* As a user I want to make a phone calls directly to some phone number from web application
* As a user I want to store customer information
* As a user I want to create separete projects for different services/products that I want to suggest to my customers
* As a user I want to store some customer phone numbers lists which I can reuse in my projects
* As a user I want to create team, add some members to my project
* As a user I want to get some metrics about the interests of my customers
